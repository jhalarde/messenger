let http = require('http').createServer()

const io = require('socket.io')(http);
const db = require('./database');

db.connection.once('open', () => {
    console.log('Connected to database');
    
    io.on('connection', socket => {
        console.log('User connected: ', socket.id)

        socket.on('SEND_MESSAGE', async ({msg, user, conversation_id}) => {

            let currUser = await db.models.User.Model.findOne({ name: user.name });

            if (!currUser) {
                currUser = await db.saveUser(new db.models.User.Model({name: user.name, socketId: socket.id}));

                await db.models.Conversation.Model.updateOne({_id: conversation_id}, {$push: {users: currUser._id}})
            }

            let createdMessage = await db.createMessage(msg, currUser, conversation_id);

            io.emit('ADD_MESSAGE', createdMessage)
        });

        socket.on('GET_MESSAGE_LIST', async convoId => {

            let messagesList = await db.getMessages(convoId);
            
            io.emit('MESSAGES_LIST', messagesList);
        });

        socket.on('ADD_NEW_CONVO', async ({convoName, userName}) => {

            let convo = await db.saveConvo(new db.models.Conversation.Model({name: convoName}));

            let currUser = await db.models.User.Model.findOne({ name: userName });

            if (!currUser) {
                currUser = await db.saveUser(new db.models.User.Model({name: userName, socketId: socket.id}));
            }

            await db.models.Conversation.Model.updateOne({_id: convo._id}, {$push: {users: currUser._id}})

            io.emit('CONVO_SAVED', await db.models.Conversation.Model.findById(convo._id).populate('users'));
        });

        socket.on('GET_CONVO_LIST', async () => {

            let convoList = await db.getConversations();

            io.emit('CONVO_LIST', convoList);
        })
    });
})

http.listen(3002, () => {
    console.log('server is running on port 3002')
});