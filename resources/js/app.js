/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('socket.io-client/dist/socket.io');

window.Vue = require('vue');
import { MessageBox } from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale';
import lang from 'element-ui/lib/locale/lang/en';
locale.use(lang);
Vue.use(MessageBox);
Vue.prototype.$prompt = MessageBox.prompt;
import Messenger from './components/Messenger';

const app = new Vue({
    el: '#app',
    render: h => h(Messenger)
});
