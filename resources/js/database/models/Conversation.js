let mongoose = require('mongoose');

let conversationSchema = new mongoose.Schema({
    name: String,
    users: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
});
let Conversation = mongoose.model('Conversation', conversationSchema)

module.exports = {
    Model: Conversation,
    Schema: conversationSchema,
};