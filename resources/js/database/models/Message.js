let mongoose = require('mongoose');

let messageSchema = new mongoose.Schema({
    message: String,
    conversation: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Conversation'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
});

let Message = mongoose.model('Message', messageSchema)

module.exports = {
    Model: Message,
    Schema: messageSchema,
}