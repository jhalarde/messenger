let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    name: String,
    socketId: String,
});

let User = mongoose.model('User', userSchema);

module.exports = {
    Model: User,
    Schema: userSchema,
};