let mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/messenger', {useNewUrlParser: true});
let connection = mongoose.connection;
let models = require('./database/models');

let saveUser = async (user) => {
    return await user.save();
}
let saveMessage = async (msg) => {
    return await msg.save();
}
let saveConvo = async (convo) => {
    return await convo.save();
}
let createMessage = async (msg, user, convoId) => {
    let newMessage = await saveMessage(new models.Message.Model({ message: msg, user: user._id, conversation: convoId }));

    return await models.Message.Model.findById(newMessage._id)
            .populate('user')
}
let getMessages = async convoId => {
    return await models.Message.Model.find({conversation: convoId})
        .populate('user');
}
let getConversations = async () => {
    return await models.Conversation.Model.find({})
        .populate('users');
}

module.exports = {
    connection,
    models,
    saveUser,
    saveMessage,
    saveConvo,
    createMessage,
    getMessages,
    getConversations,
}
