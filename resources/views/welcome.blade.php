<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full font-sans antialiased">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ mix('css/app.css')}}">
        <title>Messenger</title>

    </head>
    <body class="min-w-site bg-40 text-black min-h-full h-full bg-gray-100">
        <div id="app"></div>

        <script src="{{ mix('js/app.js')}}"></script>
    </body>
</html>
